class VotesController < ApplicationController
  include Permissions
  before_action :authorize
  before_action :logged_in_user

  def like
    create_action_by_type :like
  end

  def dislike
    create_action_by_type :dislike
  end
  
  def favorite
    create_action_by_type :favorite
  end

  private
    def authorize
      @micropost = Micropost.find_by(id: params[:id])
      unless listable_by?(current_user, @micropost)
        flash[:notice] = "First followed the user"
        redirect_to user_path @micropost.user
      end
    end

    def create_action_by_type(type)
      @micropost = Micropost.find_by(id: params[:id])
      @vote = Vote.new
      @vote.create_action_by_type(current_user, @micropost, type)
      flash[:success] = "Successful #{type} reaction"
      redirect_to user_path @micropost.user
    end
end
