class RelationshipsController < ApplicationController
  before_action :logged_in_user
  before_action :user_has_private_profile, only: [:index, :accept_status, :denied_status]

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)
    @microposts = @user.micropost_by_user(current_user).paginate(page: params[:page])
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)
    @microposts = @user.micropost_by_user(current_user).paginate(page: params[:page])
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def index
    @relationships = Relationship
                         .unscoped
                         .order(created_at: :desc, status: :desc)
                         .where("follower_id = ?", current_user.id)
                         .paginate(page: params[:page])
  end

  def accept_status
    @relationship = Relationship.unscoped.find(params[:id])
    @relationship.accepted!
    redirect :accepted
  end

  def denied_status
    @relationship = Relationship.unscoped.find(params[:id])
    @relationship.denied!
    redirect :denied
  end

  private
    def user_has_private_profile
      redirect_to(root_url) unless current_user.private_profile?
    end

    def redirect(type)
      flash[:success] = "Successful #{type} status"
      redirect_to follow_request_url
    end
end
