module MicropostsHelper
  include Permissions

  def link_to_awesome_icon(icon_class = nil, url_options = nil, html_options = nil, &block)
    link_to("#{ icon_tag(icon_class) }".html_safe, url_options, html_options, &block)
  end

  private

    def icon_tag(icon_class)
      "<i class='fa #{ icon_class }'></i>"
    end

end
