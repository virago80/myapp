class Vote < ApplicationRecord
  belongs_to :micropost, class_name: "Micropost"
  belongs_to :user, class_name: "User"
  enum vote_type: [:like, :dislike, :favorite]
  validates :user_id, presence: true
  validates :micropost_id, presence: true
  validates :vote_type, presence: true

  def create_action_by_type(voter, micropost, type)
    vote_by :voter => voter, :micropost => micropost, :vote_type => type
  end

  private

    def vote_by(options = {})

      if options[:voter].nil? || options[:micropost].nil? || options[:vote_type].nil?
        return false
      end

      _votes_ = find_votes_for({
                                   :user_id      => options[:voter].id,
                                   :micropost_id => options[:micropost].id,
                               })

      if _votes_.count == 0
        vote = Vote.new(
            :user_id      => options[:voter].id,
            :micropost_id => options[:micropost].id,
            :vote_type    => options[:vote_type]
        )
      else
        vote = _votes_.last
        vote.vote_type = options[:vote_type]
      end

      vote.save!
    end

    def find_votes_for(extra_conditions = {})
      self.class.where(extra_conditions)
    end
end
