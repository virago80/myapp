class Micropost < ApplicationRecord
  belongs_to :user
  has_many :votes, dependent: :destroy

  enum visibility: [:everyone, :who_follow_me]
  default_scope -> { order(created_at: :desc)}
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true,  length: { maximum: 140 }
  validate  :picture_size

  def count_votes_by_type(type)
    votes.where(vote_type: type)
  end

  private
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end

end
