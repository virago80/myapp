Rails.application.routes.draw do

  get 'foo/bar'
  get 'foo/baz'

  get     '/help',    to: 'static_pages#help'
  get     '/about',   to: 'static_pages#about'
  get     '/contact', to: 'static_pages#contact'
  get     '/home',    to: 'static_pages#home'
  get     '/signup',  to: 'users#new'
  post    '/signup',  to: 'users#create'
  get     '/login',   to: 'sessions#new'
  post    '/login',   to: 'sessions#create'
  delete  'logout',   to: 'sessions#destroy'
  get 'follow-request', to: 'relationships#index'


  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :users
  resources :microposts
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :microposts do
    member do
      put 'like', to: 'votes#like'
      put 'dislike', to: 'votes#dislike'
      put 'favorite', to: 'votes#favorite'
    end
  end

  resources :relationships do
    member do
      put 'accept', to: 'relationships#accept_status'
      put 'denied', to: 'relationships#denied_status'
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
end
