require 'test_helper'

class UserVoteTestTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
  end

  test "add like dislike favorite vote" do
      log_in_as(@user)
      get users_path
      put like_micropost_path @micropost
      assert_equal "Successful like reaction", flash[:success]
      put dislike_micropost_path @micropost
      assert_equal "Successful dislike reaction", flash[:success]
      put favorite_micropost_path @micropost
      assert_equal "Successful favorite reaction", flash[:success]
  end

end
