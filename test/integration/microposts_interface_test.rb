require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  include ActionView::Helpers::TextHelper

  def setup
    @user = users(:michael)
  end

  test "micropost interface" do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    post microposts_path, micropost: { content: "" }
    assert_select 'div#error_explanation'
    content = "This micropost really ties the room together"
    picture = fixture_file_upload('test/fixtures/files/rails.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, micropost: { content: content, picture: picture}
    end

    follow_redirect!
    assert_match content, response.body
    assert_select 'a', 'delete'
    first_micropost = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_micropost)
    end
    get user_path(users(:archer))
    assert_select 'a', { text: 'delete', count: 0 }
  end

  test "micropost sidebar count" do
    log_in_as(@user)
    get root_path
    micropost_count = pluralize(@user.microposts.count, "micropost")
    assert_match micropost_count, response.body
    # User with zero microposts
    other_user = users(:malory)
    log_in_as(other_user)
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "A micropost")
    get root_path
    assert_match @user.microposts.count.to_s, response.body
  end

  test "micropost permission" do
    other_user = users(:otto)
    log_in_as(other_user)
    get root_path
    other_user.microposts.create!(content: "A micropost permission", visibility: :who_follow_me)
    log_in_as(@user)
    get user_path(other_user)
    assert_select "div#microposts", text: ""
    @user.follow(other_user)
    get user_path(other_user)
    assert_match "Microposts (1)", response.body
  end
end
