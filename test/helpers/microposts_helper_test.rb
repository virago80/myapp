require 'test_helper'

class MicropostsHelperTest < ActionView::TestCase

  test "awsome_link render link with icon" do
    expected = "<a href=\"www.google.com\"><i class='fa arrow-circle-o-up'></i></a>"
    assert expected == link_to_awesome_icon('arrow-circle-o-up', 'www.google.com')
  end

end