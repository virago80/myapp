require 'test_helper'

class VoteTest < ActiveSupport::TestCase

  def setup
    @vote = Vote.new(user_id: users(:michael).id,
                     micropost_id: microposts(:orange).id,
                    vote_type: :like)
  end

  test "should be valid" do
    assert @vote.valid?
  end

  test "should require a user_id" do
    @vote.user_id = nil
    assert_not @vote.valid?
  end

  test "should require a micropost_id" do
    @vote.micropost_id = nil
    assert_not @vote.valid?
  end

  test "should require a vote_type" do
    @vote.vote_type = nil
    assert_not @vote.valid?
  end

end
