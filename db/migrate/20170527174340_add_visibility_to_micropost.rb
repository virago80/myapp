class AddVisibilityToMicropost < ActiveRecord::Migration[5.0]
  def change
    add_column :microposts, :visibility, :integer
  end
end
