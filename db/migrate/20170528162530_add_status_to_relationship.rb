class AddStatusToRelationship < ActiveRecord::Migration[5.0]
  def change
    add_column :relationships, :status, :integer, :null => false, :default => 0
  end
end
